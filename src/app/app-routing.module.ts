import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { GuardedComponent } from './components/guarded/guarded.component';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [
  { path: '', redirectTo: '/guarded', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'guarded', component: GuardedComponent, 
    canActivate: [AdminGuard] 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
