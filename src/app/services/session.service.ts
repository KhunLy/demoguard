import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user';
import * as jwt from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class SessionService {
  private user$: BehaviorSubject<User>;
  private token$: BehaviorSubject<string>;
  
  public get user(): Observable<User> {
    return this.user$.asObservable();
  }

  public get token(): Observable<string> {
    return this.token$.asObservable();
  }

  public get userValue(): User { 
    return this.user$.value; 
  }

  public get tokenValue(): string { 
    return this.token$.value; 
  }
  
  constructor() {
    let token = localStorage.getItem('TOKEN');
    if(token == null)
      this.token$ = new BehaviorSubject<string>(null);
    else
      this.token$ = new BehaviorSubject<string>(token);

    let user = localStorage.getItem('USER');
    if(user == null)
      this.user$ = new BehaviorSubject<User>(null);
    else
      this.user$ = new BehaviorSubject<User>(JSON.parse(user));
  }

  open(token: string) {
    localStorage.setItem('TOKEN', token);
    this.token$.next(token);
    // sur base du token decoder le token en un user
    let decoded = jwt(token);
    let user : User = {
      Id: decoded['Id'],
      Pseudo: decoded['Pseudo'],
      RoleName: decoded['RoleName']
    };
    localStorage.setItem('USER', JSON.stringify(user));
    this.user$.next(user);
  }

  close() {
    localStorage.clear();
    this.token$.next(null);
    this.user$.next(null);
  }
}
