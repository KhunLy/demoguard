import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user: User;

  constructor(
    private sessionService : SessionService
  ) { }

  ngOnInit(): void {
    this.sessionService.user.subscribe(data => {
      this.user = data;
    });
  }

  click1() {
    this.sessionService.open('eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJJZCI6IjEiLCJSb2xlTmFtZSI6IkFETUlOIiwiUHNldWRvIjoiTWFydGluZSIsIm5iZiI6MTU5ODk1MjU5OCwiZXhwIjoxNTk5MDM4OTk4LCJpc3MiOiJ0b2tlbkFwaSIsImF1ZCI6InRvdXRsZW1vbmRlIn0.jYKXnnM6bSsNlByuaCAnmM4i1kltckLIAigP7wty1NZVR0pnu6Ev0OBwMwnqLBFfOuglyarY13XNA3HhEb1Hww');
  }

  click2() {
    this.sessionService.close();
  }

}
