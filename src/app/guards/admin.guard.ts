import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SessionService } from '../services/session.service';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  
  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    //récupérer le user connecté
    let user : User = this.sessionService.userValue;
    //si pas de user
    if(user == null) {
      //redirection vers login
      this.router.navigateByUrl('/login');
      //retourner false
      return false;
    }
    //récupérer le role du user
    //si mauvais role
    if(user.RoleName != 'ADMIN') {
      //redirection vers ...
      //retourner false 
      return false;
    }
      
    return true;
  }
  
}
